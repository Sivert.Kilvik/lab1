package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.print.event.PrintJobListener;

public class RockPaperScissors {

	public static void main(String[] args) {
    	/*
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    boolean continuePlaying = true;

    /**
     * @humanChoice the pick the player choices
     * @compChoice the pick the computer takes
     * @winner - who won the round
     */
    public void run() { // TODO: adjust HumanChoice
        do {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");  // If (input =! rpsChoices) throw exeption

            Random rand = new Random();
            String compChoice = rpsChoices.get(rand.nextInt(3));

            /* rps LOGIC */
            String winner;
            //draw
            if (humanChoice.equals(compChoice)) {
                winner = "It's a Tie!";
                roundCounter +=1;
            }
            //human win
            else if (humanChoice.equals("rock") && compChoice.equals(rpsChoices.get(2))){
                winner = "Human wins!";
                humanScore +=1;
                roundCounter +=1;
            }
            else if (humanChoice.equals("paper") && compChoice.equals(rpsChoices.get(0))){
                winner = "Human wins!";
                humanScore +=1;
                roundCounter +=1;
            }
            else if (humanChoice.equals("scissors") && compChoice.equals(rpsChoices.get(1))){
                winner = "Human wins!";
                humanScore +=1;
                roundCounter +=1;
            }
            //human loss
            else {
                winner = "Computer wins!";
                roundCounter +=1;
                computerScore +=1;
            }

            System.out.println("Human chose "+ humanChoice + ", computer chose " + compChoice+". " + winner);

            System.out.println("Score: human " +humanScore+", computer "+computerScore);

            // CONTINUE PLAYING (y/n)?
                // TODO maybe lowercase keepPlaying(?)
            String KeepPlaying = readInput("Do you wish to continue playing? (y/n)?");
            if (KeepPlaying.equals("n") || KeepPlaying.equals("N")){
                System.out.println("Bye bye :)");
                continuePlaying = false;
            }
            else if (KeepPlaying.equals("y") || KeepPlaying.equals("Y"))
                continuePlaying = true;
            else {
                System.out.println("try again");
                //TODO Stop KeepPlaying from running

            }

        } while (continuePlaying);
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
